package edu.illinois.cs.cogcomp.twitter;

import com.twitter.hbc.core.endpoint.Location;
import edu.illinois.cs.cogcomp.lbjava.classify.Classifier;
import edu.illinois.cs.cogcomp.twitter.lbj.SentimentClassifier;

import java.util.Arrays;
import java.util.List;

public class ClassifierClient {

    public static void main(String[] args) {
        // Set up location filters
        List<Location> locations = Arrays.asList(Locations.URBANA_CHAMPAIGN, Locations.EDINBURGH);
        // Set up search-term filters
        //List<String> terms = Arrays.asList("machine learning", "natural language processing");
        // Set up language filters
        //List<String> languages = Arrays.asList("en", "es");

        TwitterClient client = new TwitterClient(null, locations, null);

        Classifier classifier = new SentimentClassifier();
        // A separate thread for handling the queue of tweets
        ClassifierMessageHandler messageHandler = new ClassifierMessageHandler(client.getMsgQueue(), client.getClient(), classifier);
        Thread thread = new Thread(messageHandler);
        thread.start();
    }
}
