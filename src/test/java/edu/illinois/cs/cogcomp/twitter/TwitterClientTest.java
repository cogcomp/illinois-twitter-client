package edu.illinois.cs.cogcomp.twitter;

import com.twitter.hbc.core.endpoint.Location;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Basic test: Receive two random tweets from New York and make sure there is a username
 */
public class TwitterClientTest {

    private TwitterClient client;

    @Before
    public void setUp() throws Exception {
        List<Location> locations = Collections.singletonList(Locations.NEW_YORK);
        client = new TwitterClient(null, locations, null);
    }

    @After
    public void tearDown() throws Exception {
        client.getClient().stop();
    }

    @Test
    public void testGetMessage() throws Exception {
        int messagesReceived = 0;
        while (messagesReceived < 2) {
            String msg = client.getMsgQueue().take();
            JSONObject tweet = new JSONObject(msg);
            assertTrue(!Utils.getUser(tweet).getName().isEmpty());
            Utils.printInfo(tweet);
            messagesReceived++;
        }
    }
}